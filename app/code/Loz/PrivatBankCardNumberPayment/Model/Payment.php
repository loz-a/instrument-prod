<?php

namespace Loz\PrivatBankCardNumberPayment\Model;

use Magento\Payment\Model\Method\AbstractMethod;

class Payment extends AbstractMethod
{
    const PAYMENT_METHOD_PRIVATBANK_CARD_NUMBER_CODE = 'privatbank_card_number';

    protected $_code = self::PAYMENT_METHOD_PRIVATBANK_CARD_NUMBER_CODE;
}
