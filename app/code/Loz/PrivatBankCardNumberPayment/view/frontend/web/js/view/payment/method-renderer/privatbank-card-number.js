define([
    'ko',
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/model/payment-service',
    'Loz_PrivatBankCardNumberPayment/js/model/shipping-info-service',
    'Loz_PrivatBankCardNumberPayment/js/action/place-order'
],
function(
    ko,
    $,
    PaymentDefault,
    paymentService,
    shippingInfoService,
    placeOrderAction
) {
    'use strict';
    
    return PaymentDefault.extend({
        defaults: {
            template: 'Loz_PrivatBankCardNumberPayment/payment/privatbank-card-number'
        },

        initialize: function() {
            this._super();

            if (paymentService.isOnlyOneAvailablePaymentMethod()) {
                this.selectPaymentMethod();
            }
        },

        getPlaceOrderDeferredObject: function () {
            return $.when(
                placeOrderAction(this.getData(), this.messageContainer)
            );
        },

        placeOrder: function (data, event) {
            if (shippingInfoService.validateShippingInfo()) {
                return this._super(data, event);
            }
            return false;
        }

    });
});