define([
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-save-processor/payload-extender',
    'Magento_Checkout/js/action/select-billing-address',
    'uiRegistry'
], function(
    quote,
    payloadExtender,
    selectBillingAddressAction,
    registry
) {
    'use strict';

    return {
        getShippingInfo: function () {
            var shippingInfo;

            // if (!quote.billingAddress()) {
                selectBillingAddressAction(quote.shippingAddress());
            // }

            shippingInfo = {
                addressInformation: {
                    'shipping_address': quote.shippingAddress(),
                    'billing_address': quote.billingAddress(),
                    'shipping_method_code': quote.shippingMethod()['method_code'],
                    'shipping_carrier_code': quote.shippingMethod()['carrier_code']
                }
            };

            payloadExtender(shippingInfo);
            return shippingInfo.addressInformation;
        },

        validateShippingInfo: function() {
            return registry.get('checkout.shippingAddress').validateShippingInformation();
        }
    };

});