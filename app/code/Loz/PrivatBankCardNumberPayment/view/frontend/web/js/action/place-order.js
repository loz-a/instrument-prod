/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/place-order',
    'Loz_PrivatBankCardNumberPayment/js/model/shipping-info-service'
],
function (
    quote,
    urlBuilder,
    customer,
    placeOrderService,
    shippingInfoService
) {
    'use strict';

    return function (paymentData, messageContainer) {
        var serviceUrl, payload;
        var shippingInfo = shippingInfoService.getShippingInfo();

        payload = {
            cartId: quote.getQuoteId(),
            billselecingAddress: quote.billingAddress(),
            paymentMethod: paymentData,
            addressInformation: shippingInfo
        };

        if (customer.isLoggedIn()) {
            serviceUrl = urlBuilder.createUrl('/carts/mine/place-order', {});
        } else {
            serviceUrl = urlBuilder.createUrl('/guest-carts/:quoteId/place-order', {
                quoteId: quote.getQuoteId()
            });
            payload.email = quote.guestEmail;
        }

        return placeOrderService(serviceUrl, payload, messageContainer);
    };
});
