define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
], 
function(Component, rendererList) {
    'use strict';
    
    rendererList.push(
        {
            type: 'privatbank_card_number',
            component: 'Loz_PrivatBankCardNumberPayment/js/view/payment/method-renderer/privatbank-card-number'
        }
    );
    
    return Component.extend({});
});