define([], function () {
    'use strict';

    return function(paymentService) {

        paymentService.isOnlyOneAvailablePaymentMethod = function() {
            return this.getAvailablePaymentMethods().length === 1;
        };

        return paymentService;
    };
});