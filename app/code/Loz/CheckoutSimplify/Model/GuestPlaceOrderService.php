<?php

namespace Loz\CheckoutSimplify\Model;

use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Api\{ShippingInformationManagementInterface, PaymentInformationManagementInterface};
use Magento\Quote\Api\{GuestBillingAddressManagementInterface, GuestPaymentMethodManagementInterface, GuestCartManagementInterface, CartRepositoryInterface};
use Magento\Quote\Api\Data\{PaymentInterface, AddressInterface};
use Loz\CheckoutSimplify\Api\GuestPlaceOrderInterface;
use Magento\Framework\Exception\{LocalizedException, CouldNotSaveException};
use Exception;

class GuestPlaceOrderService implements GuestPlaceOrderInterface
{
    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    protected $quoteIdMask;

    /**
     * @var \Magento\Checkout\Api\ShippingInformationManagementInterface
     */
    protected $shippingInformationManagement;

    /**
     * @var \Magento\Quote\Api\GuestBillingAddressManagementInterface
     */
    protected $billingAddressManagement;

    /**
     * @var \Magento\Quote\Api\GuestPaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    /**
     * @var \Magento\Quote\Api\GuestCartManagementInterface
     */
    protected $cartManagement;

    /**
     * @var \Magento\Checkout\Api\PaymentInformationManagementInterface
     */
    protected $paymentInformationManagement;


    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * GuestPlaceOrderService constructor.
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param ShippingInformationManagementInterface $shippingInformationManagement
     * @param GuestBillingAddressManagementInterface $billingAddressManagement
     * @param GuestPaymentMethodManagementInterface $paymentMethodManagement
     * @param GuestCartManagementInterface $cartManagement
     * @param PaymentInformationManagementInterface $paymentInformationManagement
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        QuoteIdMaskFactory $quoteIdMaskFactory,
        ShippingInformationManagementInterface $shippingInformationManagement,
        GuestBillingAddressManagementInterface $billingAddressManagement,
        GuestPaymentMethodManagementInterface $paymentMethodManagement,
        GuestCartManagementInterface $cartManagement,
        PaymentInformationManagementInterface $paymentInformationManagement,
        CartRepositoryInterface $cartRepository
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->billingAddressManagement = $billingAddressManagement;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->cartManagement = $cartManagement;
        $this->paymentInformationManagement = $paymentInformationManagement;
        $this->cartRepository = $cartRepository;
    }


    /**
     * @param string $cartId
     * @param string $email
     * @param ShippingInformationInterface $addressInformation
     * @param PaymentInterface $paymentMethod
     * @param AddressInterface|null $billingAddress
     * @return int
     * @throws CouldNotSaveException
     */
    public function placeOrder(
        $cartId,
        $email,
        ShippingInformationInterface $addressInformation,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    )
    {
        $this->saveShippingInfo($cartId, $addressInformation);
        $this->savePaymentInfo($cartId, $email, $paymentMethod, $billingAddress);

        try {
            $orderId = $this->cartManagement->placeOrder($cartId);
        } catch (LocalizedException $e) {
            throw new CouldNotSaveException(__($e->getMessage()), $e);
        } catch (Exception $e) {
            $this->logger->critical($e);
            throw new CouldNotSaveException(
                __('An error occurred on the server. Please try to place the order again.'),
                $e
            );
        }

        return $orderId;
    }


    protected function saveShippingInfo($cartId, ShippingInformationInterface $addressInformation)
    {
        $quoteId = $this->getQuoteIdMask($cartId)->getQuoteId();
        return $this->shippingInformationManagement->saveAddressInformation(
            $quoteId,
            $addressInformation
        );
    }


    protected function savePaymentInfo(
        $cartId,
        $email,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    ) {
        if ($billingAddress) {
            $billingAddress->setEmail($email);
            $this->billingAddressManagement->assign($cartId, $billingAddress);
        } else {
            $quoteId = $this->getQuoteIdMask($cartId)->getQuoteId();
            $this->cartRepository->getActive($quoteId)->getBillingAddress()->setEmail($email);
        }

        $this->paymentMethodManagement->set($cartId, $paymentMethod);
        return true;
    }


    protected function getQuoteIdMask($cartId)
    {
        if (null === $this->quoteIdMask) {
            $this->quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        }
        return $this->quoteIdMask;
    }

}