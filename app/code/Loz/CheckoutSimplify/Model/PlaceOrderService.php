<?php

namespace Loz\CheckoutSimplify\Model;

use Loz\CheckoutSimplify\Api\PlaceOrderInterface;
use Magento\Quote\Api\Data\{PaymentInterface, AddressInterface};
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Checkout\Api\{ShippingInformationManagementInterface, PaymentInformationManagementInterface};
use Magento\Quote\Api\{BillingAddressManagementInterface, PaymentMethodManagementInterface, CartManagementInterface, CartRepositoryInterface};
use Magento\Framework\Exception\{LocalizedException, CouldNotSaveException};
use Exception;

class PlaceOrderService implements PlaceOrderInterface
{
    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    protected $quoteIdMask;

    /**
     * @var \Magento\Checkout\Api\ShippingInformationManagementInterface
     */
    protected $shippingInformationManagement;

    /**
     * @var \Magento\Quote\Api\BillingAddressManagementInterface
     */
    protected $billingAddressManagement;

    /**
     * @var \Magento\Quote\Api\PaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagement;

    /**
     * @var \Magento\Checkout\Api\PaymentInformationManagementInterface
     */
    protected $paymentInformationManagement;


    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * GuestPlaceOrderService constructor.
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param ShippingInformationManagementInterface $shippingInformationManagement
     * @param BillingAddressManagementInterface $billingAddressManagement
     * @param PaymentMethodManagementInterface $paymentMethodManagement
     * @param CartManagementInterface $cartManagement
     * @param PaymentInformationManagementInterface $paymentInformationManagement
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        QuoteIdMaskFactory $quoteIdMaskFactory,
        ShippingInformationManagementInterface $shippingInformationManagement,
        BillingAddressManagementInterface $billingAddressManagement,
        PaymentMethodManagementInterface $paymentMethodManagement,
        CartManagementInterface $cartManagement,
        PaymentInformationManagementInterface $paymentInformationManagement,
        CartRepositoryInterface $cartRepository
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->billingAddressManagement = $billingAddressManagement;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->cartManagement = $cartManagement;
        $this->paymentInformationManagement = $paymentInformationManagement;
        $this->cartRepository = $cartRepository;
    }


    /**
     * @param string $cartId
     * @param ShippingInformationInterface $addressInformation
     * @param PaymentInterface $paymentMethod
     * @param AddressInterface|null $billingAddress
     * @return int
     * @throws CouldNotSaveException
     */
    public function placeOrder(
        $cartId,
        ShippingInformationInterface $addressInformation,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    )
    {
        $this->saveShippingInfo($cartId, $addressInformation);
        $this->savePaymentInfo($cartId, $paymentMethod, $billingAddress);

        try {
            $orderId = $this->cartManagement->placeOrder($cartId);
        } catch (LocalizedException $e) {
            throw new CouldNotSaveException(__($e->getMessage()), $e);
        } catch (Exception $e) {
            $this->logger->critical($e);
            throw new CouldNotSaveException(
                __('An error occurred on the server. Please try to place the order again.'),
                $e
            );
        }

        return $orderId;
    }


    protected function saveShippingInfo($cartId, ShippingInformationInterface $addressInformation)
    {
        return $this->shippingInformationManagement->saveAddressInformation(
            $cartId,
            $addressInformation
        );
    }


    protected function savePaymentInfo(
        $cartId,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    ) {
        if ($billingAddress) {
            $this->billingAddressManagement->assign($cartId, $billingAddress);
        } else {
            $quote = $this->cartRepository->getActive($cartId);
            $shippingAddress = $quote->getShippingAddress();
            $email = $shippingAddress->getEmail();
            $quote->getBillingAddress()->setEmail($email);
        }
        $this->paymentMethodManagement->set($cartId, $paymentMethod);
        return true;
    }


//    protected function getQuoteIdMask($cartId)
//    {
//        if (null === $this->quoteIdMask) {
//            $this->quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
//        }
//        return $this->quoteIdMask;
//    }

}