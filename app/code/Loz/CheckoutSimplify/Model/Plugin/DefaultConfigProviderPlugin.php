<?php

namespace Loz\CheckoutSimplify\Model\Plugin;

use Magento\Checkout\Model\DefaultConfigProvider as ConfigProvider;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Api\PaymentMethodManagementInterface;

class DefaultConfigProviderPlugin
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var PaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    public function __construct(
        CheckoutSession $checkoutSession,
        PaymentMethodManagementInterface $paymentMethodManagement
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->paymentMethodManagement = $paymentMethodManagement;
    }    
    
    
    public function afterGetConfig(ConfigProvider $configProvider, $result)
    {
        $quote = $this->checkoutSession->getQuote();
        if (!$quote->getIsVirtual()) {
            $result['paymentMethods'] = $this->getPaymentMethods();
        }

        return $result;
    }
    
    
    protected function getPaymentMethods()
    {
        $paymentMethods = [];
        $quote = $this->checkoutSession->getQuote();

        foreach ($this->paymentMethodManagement->getList($quote->getId()) as $paymentMethod) {
            $paymentMethods[] = [
                'code' => $paymentMethod->getCode(),
                'title' => __($paymentMethod->getTitle())
            ];
        }

        return $paymentMethods;
    }
    
}
