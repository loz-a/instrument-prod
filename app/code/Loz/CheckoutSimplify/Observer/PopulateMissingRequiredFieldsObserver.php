<?php

namespace Loz\CheckoutSimplify\Observer;

use Magento\Framework\Event\{Observer, ObserverInterface};
use Magento\Framework\App\Config\ScopeConfigInterface;

class PopulateMissingRequiredFieldsObserver implements ObserverInterface
{
    const XML_PATH_DEFAULT_COUNTRY = 'general/country/default';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * PopulateMissingRequiredFieldsObserver constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }


    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getData('quote');

        $defaultCountryId = $this->scopeConfig->getValue(self::XML_PATH_DEFAULT_COUNTRY);

        $quote
            ->getShippingAddress()
            ->setStreet('n/a')
            ->setCountryId($defaultCountryId);

        $quote
            ->getBillingAddress()
            ->setStreet('n/a')
            ->setCountryId($defaultCountryId);
    }

}