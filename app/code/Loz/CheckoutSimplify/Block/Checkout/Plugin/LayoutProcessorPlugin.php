<?php
namespace Loz\CheckoutSimplify\Block\Checkout\Plugin;

use Magento\Checkout\Block\Checkout\LayoutProcessor;

class LayoutProcessorPlugin
{
    public function aroundProcess(LayoutProcessor $subject, callable $proceed, $jsLayout)
    {
        $jsLayout['components']['checkout']['children']['steps'] = ['children' => []];        
        
        $stepsChildren = &$jsLayout['components']['checkout']['children']['steps']['children'];
        
        if (isset($jsLayout['components']['checkout']['children']['payment']['children'])) {
            $stepsChildren['billing-step'] = [
                'children' => [
                    'payment' => [
                        'children' => $jsLayout['components']['checkout']['children']['payment']['children']
                    ]
                ]
            ];
        }
        
        if (isset($jsLayout['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
            $shippingAdressFieldset = &$jsLayout['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
            
            $stepsChildren['shipping-step'] = [
                'children' => [
                    'shippingAddress' => [
                        'children' => [
                            'shipping-address-fieldset' => [
                                'children' => $shippingAdressFieldset
                            ]
                        ]
                    ]
                ]
            ];            
        }
        
        $result = $proceed($jsLayout);
        
//        if (isset($result['components']['checkout']['children']['payment']['children'])) {
//            if (isset($result['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children'])) {
//                $result['components']['checkout']['children']['payment']['children'] = array_merge_recursive(
//                    $result['components']['checkout']['children']['payment']['children'],
//                    $result['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
//                );
//            }
//        }
        
        if (isset($result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
            if (isset($result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
                $result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'] = array_merge_recursive(
                    $result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'],
                    $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']
                );
            }
            
            unset(
                $result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['postcode'],
                $result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['street'],
                $result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['country_id'],
                $result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['region_id'],
                $result['components']['checkout']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['region'],
                $result['components']['checkout']['children']['sidebar']['children']['shipping-information']
            );
            
        }

        
        if(isset($result['components']['checkout']['children']['steps'])) {
            unset($result['components']['checkout']['children']['steps']);
        }
        
        return $result;
    }
}
